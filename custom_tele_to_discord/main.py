import json
import os
import requests
import logging
import pprint
import telethon.sync
from telethon import TelegramClient
from telethon.tl.functions.messages import GetDialogsRequest, GetHistoryRequest
from telethon.tl.functions.channels import GetFullChannelRequest
from telethon.tl.functions.updates import GetChannelDifferenceRequest
from telethon.tl.types import UpdateShortMessage, UpdateNewChannelMessage, PeerUser, PeerChat, PeerChannel, InputPeerEmpty, Channel, ChannelMessagesFilter, ChannelMessagesFilterEmpty
from time import sleep
from discord_hooks import Webhook
from discord_webhook import DiscordWebhook

channel_names = []
channel_ids_new = []
channel_info = []


with open('config.json') as config_file:
    config = json.load(config_file)


FORMAT = '%(asctime)-15s %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.INFO,format=FORMAT,datefmt='%m-%d-%Y %H:%M')
logger = logging.getLogger('teledisc')

try:
    url = config['discord']['url']
    api_id = config['telegram']['api_id']
    api_hash = config['telegram']['api_hash']
    phone = config['telegram']['phone']
    channel_id = config['telegram']['channel_ids']
    everyone = config['telegram']['everyone']
    loglevel = config['telegram']['loglevel']
except:
    logger.error('Error processing config file')

logger.setLevel(loglevel)

client = TelegramClient('anon',api_id,api_hash)
client.connect()

if not client.is_user_authorized():
    client.send_code_request(phone)
    code = input(f'Enter code sent to {phone} : ')
    me = client.sign_in(phone,code)


lastmessage = 0
last_date = None
chunk_size = 20
chan_type = 'channel'
result = client(GetDialogsRequest(
                 offset_date=last_date,
                 offset_id=0,
                 offset_peer=InputPeerEmpty(),
                 limit=chunk_size,
                 hash =0
             ))
pprint.pprint(result)
print("\nAvailable channels/groups are : ")
for i in result.chats:
    print(f'{i.id} : {i.title}')
    for j in channel_id:
        if i.id == j:
            channel_names.append(i.title) 
            channel_info.append(i)
            # rawr = i
            # print(i)
            
# print(f'\n{rawr.title}\n{rawr.stringify()}')
for x in range(len(channel_info)):
    print(channel_names[x])
    print(channel_info[x].stringify())


try:
    logger.info("\nListening for messages from channel {} with ID {}".format(channel_name,channel_id))
except:
    logger.error("Whoops! Couldn't find channel ID '{}'".format(channel_id))




for his_x in range(len(channel_id)):
    history = client(GetHistoryRequest(peer=client.get_input_entity(PeerChannel(channel_id[his_x])),
                                                offset_date=last_date,
                                                offset_id=0,
                                                add_offset=0,
                                                limit=10,
                                                max_id=0,
                                                min_id=0,
                                                hash=0
                                            ))
    history.messages.reverse()
    logger.info("\n\nLast 10 Messages:\n")
    for m in history.messages:
        datetime = m.date.strftime('%Y-%m-%d %H:%M:%S')
        try:
            logger.info(datetime+" "+str(m.id)+": "+m.message)
        except:
            continue
        if m.id > lastmessage:
            lastmessage = m.id
        try:
            logger.info("Relaying Message {}".format(m.id))
            # print(len(history.message))
            media = m.media
            if media is not None:
                logger.info("Would download image")
                logger.debug(media)
            if not m.message == '':
                if everyone:
                    msgText = "@noteveryone {}".format(m.message)
                else:
                    msgText = "{}".format(m.message)
        except:
            logger.info('Ignoring empty message {} action: {}'.format(m.id, m.action))
        try:
            logger.info(datetime+" "+str(m.id)+": "+m.message)
        except:
            logger.debug(m)



# while True:
#     try:
#         for his_x in range(len(channel_id)):
#             messages = client(GetHistoryRequest(peer=client.get_input_entity(PeerChannel(channel_id[his_x])),
#                                                         offset_date=last_date,
#                                                         offset_id=0,
#                                                         add_offset=0,
#                                                         limit=10,
#                                                         max_id=0,
#                                                         min_id=lastmessage,
#                                                         hash=0
#                                                     ))
                                            
#             if len(messages.messages)>0:
#                 logger.debug('New Message: ')
#                 logger.debug(messages)
#                 for m in messages.messages:
#                     datetime = m.date.strftime('%Y-%m-%d %H:%M:%S')
#                     if m.id > lastmessage:
#                         lastmessage = m.id
                    
for his_x in range(len(channel_id)):
    while True:
        try:
            messages = client(GetHistoryRequest(peer=client.get_input_entity(PeerChannel(channel_id[his_x])),
                                                offset_date=last_date,
                                                offset_id=0,
                                                add_offset=0,
                                                limit=50,
                                                max_id=0,
                                                min_id=lastmessage,
                                                hash=0
                                            ))
           
            if len(messages.messages) > 0:
                logger.debug('New Messages: ')
                logger.debug(messages)
                for m in messages.messages:
                    datetime = m.date.strftime('%Y-%m-%d %H:%M:%S')
                    if m.id > lastmessage:
                        lastmessage = m.id
                    try:
                        logger.info("Relaying Message {}".format(m.id))
                        # media = m.media
                        # if media is not None:
                        #     logger.info("Will download image")
                        #     logger.debug(media)
                        #     download_res = tclient.download_media(
                        #         media, './downloads/')
                        #     logger.info("Download done: {}".format(download_res))
                        #     files = {'file': (open(download_res, 'rb'))}
                        #     response = requests.post(url, files=files)
                        #     logger.debug(response.text)
                        #     os.remove(download_res)
                        #     logger.debug("File deleted")
                        if not m.message == '':
                            msgText = "@everyone {}".format(m.message)
                            msg = Webhook(url,title = "Predictum indicator",desc = msgText,author="Drake")
                            # msg.set_title("idk bro")
                            # msg = DiscordWebhook(url=url, username="Drake")
                            # embed = DiscordEmbed(title='Predictum', description=msgText, color=242424)
                            # embed.set_author(name='hosmail', url='https://github.com/arjun069', icon_url='https://avatars0.githubusercontent.com/u/14542790')
                            # msg.add_embed(embed)
                            # response = msg.execute()
                            msg.post() 
                    except:
                        logger.info('Ignoring empty message {} action: {}'.format(m.id, m.action))
                    try:
                        logger.info(datetime+" "+str(m.id)+": "+m.message)
                    except:
                        logger.debug(m)
            sleep(2)
            continue
        except KeyboardInterrupt:
            break

client.disconnect()